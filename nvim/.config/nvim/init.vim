" TODO cleanup old plugins, use LSP for completion
let mapleader=" "

" set termguicolors

set number relativenumber
set tabstop=4
set shiftwidth=4
set encoding=utf-8

" tab complete in command mode
set wildmode=longest,list,full
set splitbelow splitright

filetype plugin indent on
syntax enable

" plugins
if &compatible
	set nocompatible
endif
filetype off
" append to runtime path
set rtp+=/usr/share/vim/vimfiles

" LOAD PLUGINS
" initialize dein, plugins are installed to this directory
call dein#begin(expand('~/.cache/dein'))
" add packages here, e.g:
call dein#add('vifm/vifm.vim')
call dein#add('lervag/vimtex')
call dein#add('neovim/nvim-lspconfig')
call dein#add('hrsh7th/cmp-nvim-lsp')
call dein#add('hrsh7th/cmp-buffer')
call dein#add('hrsh7th/cmp-path')
call dein#add('hrsh7th/cmp-cmdline')
call dein#add('hrsh7th/nvim-cmp')
call dein#add('hrsh7th/cmp-vsnip')
call dein#add('hrsh7th/vim-vsnip')
call dein#add('iagoleal/doctor.nvim')
"call dein#add('Shougo/deoplete.nvim')
"call dein#add('deoplete-plugins/deoplete-lsp')
" use vimtex for tex completion
"call deoplete#custom#var('omni', 'input_patterns', {
"	\ 'tex': g:vimtex#re#deoplete
"	\})
"call dein#add('Shougo/neco-syntax')
"call dein#add('deoplete-plugins/deoplete-clang')
"call dein#add('deoplete-plugins/deoplete-jedi')
"call dein#add('sbdchd/neoformat')
call dein#add('unblevable/quick-scope')
"call dein#add('rrethy/vim-hexokinase', { 'build': 'make hexokinase' })
if !has('nvim')
	" no idea what this does
endif
" exit dein
call dein#end()
" auto-install missing packages on startup
if dein#check_install()
	call dein#install()
endif


lua <<EOF
  -- Setup nvim-cmp.
  local cmp = require'cmp'

  cmp.setup({
    snippet = {
      -- REQUIRED - you must specify a snippet engine
      expand = function(args)
        vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
        -- require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
        -- vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
        -- require'snippy'.expand_snippet(args.body) -- For `snippy` users.
      end,
    },
    mapping = {
      ['<C-b>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
      ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
      ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
      ['<C-y>'] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
      ['<C-e>'] = cmp.mapping({
        i = cmp.mapping.abort(),
        c = cmp.mapping.close(),
      }),
      ['<CR>'] = cmp.mapping.confirm({ select = true }),
    },
    sources = cmp.config.sources({
      { name = 'nvim_lsp' },
      { name = 'vsnip' }, -- For vsnip users.
      -- { name = 'luasnip' }, -- For luasnip users.
      -- { name = 'ultisnips' }, -- For ultisnips users.
      -- { name = 'snippy' }, -- For snippy users.
    }, {
      { name = 'buffer' },
    })
  })

  -- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
  cmp.setup.cmdline('/', {
    sources = {
      { name = 'buffer' }
    }
  })

  -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
  cmp.setup.cmdline(':', {
    sources = cmp.config.sources({
      { name = 'path' }
    }, {
      { name = 'cmdline' }
    })
  })

  -- Setup lspconfig.
  local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
  -- Replace <YOUR_LSP_SERVER> with each lsp server you've enabled.
  require('lspconfig')['vimls'].setup {
    capabilities = capabilities
  }
  require('lspconfig')['bashls'].setup {
    capabilities = capabilities
  }
  require('lspconfig')['clangd'].setup {
    capabilities = capabilities
  }
  --require('lspconfig')['ccls'].setup {
  --  capabilities = capabilities
  --}
  require('lspconfig')['pylsp'].setup {
    capabilities = capabilities
  }
  require('lspconfig')['denols'].setup {
    capabilities = capabilities
  }
  require('lspconfig')['html'].setup {
    capabilities = capabilities
  }
  require('lspconfig')['cssls'].setup {
    capabilities = capabilities
  }
  require('lspconfig')['texlab'].setup {
    capabilities = capabilities
  }
  require('lspconfig')['zeta_note'].setup {
    capabilities = capabilities
  }
  require('lspconfig')['jsonls'].setup {
    capabilities = capabilities
  }
EOF

let g:Hexokinase_highlighters = ['backgroundfull']

" Trigger a highlight in the appropriate direction when pressing these keys:
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']

highlight QuickScopePrimary guifg='#ef9849' gui=underline ctermfg=3 cterm=underline
highlight QuickScopeSecondary guifg='#5fffff' gui=underline ctermfg=2 cterm=underline

let g:deoplete#enable_at_startup = 1
filetype plugin indent on

let g:vimtex_compiler_progname = 'nvr'
if empty(v:servername) && exists('*remote_startserver')
	call remote_startserver('VIM')
endif
set completeopt=menu,menuone,noselect
" set completeopt+=menuone,noselect,noinsert
" let g:deoplete#enable_at_startup = 1
"

" Netrw settings - TODO replicate functionality in vifm
" turn off banner
let g:netrw_banner = 0
" open files in vertical split
let g:netrw_browse_split = 2
" tree-like listing
let g:netrw_liststyle = 3
" human-readable filesizes
let g:netrw_sizestyle = 'H'
" make netrw take up less space
let g:netrw_winsize = 15
let g:netrw_altv = 1

" Custom binds

" Netrw project drawer
map <leader>v :Vexplore<CR>
" Open up vifm to select a file to open in a tab
map <leader>i :TabVifm<CR>

" split navigation without C-w (copied from Luke Smith)
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Allow saving of files as sudo when I forgot to start vim using sudo (SO)
cmap w!! w !sudo tee > /dev/null %

" close deoplete preview/popup window automatically
autocmd CompleteDone * silent! pclose!
" automatically apply changes on save in certain files
autocmd BufWritePost ~/.config/sway/config,~/dotfiles/sway/.config/sway/config !swaymsg reload
autocmd BufWritePost ~/.config/sxhkd/sxhkdrc,~/dotfiles/sxhkd/.config/sxhkd/sxhkdrc !pkill -SIGUSR1 sxhkd
autocmd BufWritePost ~/.Xresources !xrdb %
autocmd BufWritePost ~/.config/nvim/init.vim,~/dotfiles/nvim/.config/nvim/init.vim :so %
