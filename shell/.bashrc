#
# bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

set -o vi
bind -m vi-insert "\C-l":clear-screen

[[ -f ~/.config/aliases ]] && . ~/.config/aliases
[[ -f ~/.config/bash_aliases ]] && . ~/.config/bash_aliases

[[ -f /usr/share/fzf/key-bindings.bash ]] && . /usr/share/fzf/key-bindings.bash
[[ -f /usr/share/fzf/completion.bash ]] && . /usr/share/fzf/completion.bash


HISTSIZE=5000

# custom text formatting
# "Fun" tidbit. Abaout a year ago I found a neat way to indicate if you are working on a local machine or are SSHed into one
# I copied and slightly adapted the script from a website, not paying attention how it called bash with the "-l" flag
# That is the short alias for --login, meaning bash itself read my .profile
# Since I don't use a display manager and just start my graphical sessions depending on which TTY I log into (via exec)
# This meant that as soon as I quit the initial graphical session another one got started
# Back then I thought it might have been a problem with i3, however the issue persisted after switching to another window manager
# Binging this obscure issue ("arch X starts twice" and similar search queries) didn't help a bit, since it was extremely unique to my setup and I did not ever think my PS1 variable
# could possibly be responsible.
# I do not know if the --login option is necessary to determine if SSH_CLIENT is set; if it is, I won't use this detection at all

# Bug has been happening since around 2019-10, possibly sooner
# Tracked down 2020-09-22; during time which should have been spent studying
reset="\[$(tput sgr0)\]" # reset everything
uname_host="\[$(tput setaf 167)\]" # 167
misc="\[$(tput setaf 7)\]"
bold="\[$(tput bold)\]"
isssh="\[$(sh -c "env | grep -q 'SSH_CLIENT' && tput setaf 1 && printf '%s' '  '")\]"

#PS1='[\u@\h \W]\$ '
PS1="${bold}${misc}[${isssh}${uname_host}\u${reset}@${bold}${uname_host}\h \W${misc}]\$${reset} "


# BEGIN_KITTY_SHELL_INTEGRATION
if test -n "$KITTY_INSTALLATION_DIR" -a -e "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; then source "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; fi
# END_KITTY_SHELL_INTEGRATION
