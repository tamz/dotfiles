#!/bin/sh
# ~/.profile
#

# for programs that comply with the XDG Base Directory Specification (tho these are defaults anyway)
# see https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"

export PATH="$PATH:$HOME/.local/bin:$HOME/.local/bin/statusbar"
export TEXMFHOME="$XDG_CONFIG_HOME/texmf"
export TEXBIB="$XDG_DATA_HOME/biblio"
export GNUPGHOME="$XDG_CONFIG_HOME/gnupg"
export INPUTRC="$XDG_CONFIG_HOME/readline/inputrc"
export PIPEWIRE_CONFIG_FILE="$XDG_CONFIG_HOME/pipewire-media-session/default-profile"
export MAILCAPS="$XDG_CONFIG_HOME/mutt/mailcap"
export WINEPREFIX="$XDG_DATA_HOME/winepfx/default"
export TERMINAL="kitty"
export EDITOR="nvim"
export VISUAL="nvim"
export SUDO_EDITOR="nvim"
export SUDO_ASKPASS="$HOME/.local/bin/menupass"
export BROWSER="lynx"

# tesseract language data
export TESSDATA_PREFIX="/usr/share/tessdata"

# man colors
export LESS_TERMCAP_md="$(printf '\e%b' '[0;33m')"
export LESS_TERMCAP_me="$(printf '\e%b' '[0m')"
export LESS_TERMCAP_mh="$(printf '\e%b' '[02;32m')"
export LESS_TERMCAP_se="$(printf '\e%b' '[0m')"
export LESS_TERMCAP_so="$(printf '\e%b' '[01;40;37m')"
export LESS_TERMCAP_ue="$(printf '\e%b' '[0m')"
export LESS_TERMCAP_us="$(printf '\e%b' '[3;4;34m')"

# todo add to systemd session thingy
# https://wiki.archlinux.org/index.php/Qt
export QT_PLUGIN_PATH="$HOME/.kde4/lib/kde4/plugins/:/usr/lib/kde4/plugins/"
export QT_QPA_PLATFORMTHEME="qt5ct"

# fuck you poettering
systemd_import_env() {
	systemctl --user import-environment PATH EDITOR VISUAL SUDO_EDITOR SUDO_ASKPASS TERMINAL BROWSER XDG_CURRENT_DESKTOP XDG_SESSION_TYPE MOZ_ENABLE_WAYLAND QT_QPA_PLATFORM WAYLAND_DISPLAY
}

if [ "$(tty)" = "/dev/tty1" ]; then
	# export BROWSER="chromium --proxy-server=socks://127.0.0.1:9050"
	export BROWSER="qutebrowser"
	export XDG_CURRENT_DESKTOP="sway"
	export XDG_SESSION_TYPE="wayland"
	export MOZ_ENABLE_WAYLAND="1"
	# caution: some programs don't yet work with this
	export QT_QPA_PLATFORM="wayland"

	# wayland session
	# apparently this doesn't even do what I wanted it to, maybe get rid of it
	sed -i "s/XDG_CURRENT_DESKTOP=.*\$/XDG_CURRENT_DESKTOP=${XDG_CURRENT_DESKTOP}/g" "$XDG_CONFIG_HOME/environment.d/envvars.conf"
	systemd_import_env
	pgrep -x sway > /dev/null || exec dbus-run-session sway

elif [ "$(tty)" = "/dev/tty2" ]; then
	export BROWSER="qutebrowser"
	export XDG_SESSION_TYPE="wayland"
	export MOZ_ENABLE_WAYLAND="1"
	# caution: some programs don't yet work with this
	export QT_QPA_PLATFORM="wayland"

	sed -i "s/XDG_CURRENT_DESKTOP=.*\$/XDG_CURRENT_DESKTOP=${XDG_CURRENT_DESKTOP}/g" "$XDG_CONFIG_HOME/environment.d/envvars.conf"
	systemd_import_env
	pgrep -x startplasma-wayland > /dev/null || exec dbus-run-session startplasma-wayland

elif [ "$(tty)" = "/dev/tty3" ]; then
	# prevent chromium from using .config/chromium-flags.conf
	# flags causing it to not launch do to forcing wayland
	export BROWSER="/usr/lib/chromium/chromium --incognito"
	export XDG_SESSION_TYPE="x11"

	# x session
	systemd_import_env
	pgrep -x Xorg > /dev/null || exec startx

fi
