import subprocess


# TODO consider using the color values directly,
# as this would not work on a pure wayland setup

def read_xresources(prefix):

    props = {}
    x = subprocess.run(['xrdb', '-query'], stdout=subprocess.PIPE)
    lines = x.stdout.decode().split('\n')
    for line in filter(lambda l: l.startswith(prefix), lines):
        prop, _, value = line.partition(':\t')
        props[prop] = value
    return props


xresources = read_xresources('*')

c.tabs.padding = {'bottom': 2, 'left': 2, 'right': 2, 'top': 2}

c.colors.downloads.bar.bg = xresources['*background']

c.colors.tabs.bar.bg = xresources['*background']
c.colors.tabs.even.bg = xresources['*background']
c.colors.tabs.odd.bg = xresources['*fadeColor']
c.colors.tabs.even.fg = xresources['*cursorColor']
c.colors.tabs.odd.fg = xresources['*cursorColor']

c.colors.tabs.selected.even.bg = xresources['*foreground']
c.colors.tabs.selected.odd.bg = xresources['*foreground']
c.colors.tabs.selected.even.fg = xresources['*color15']
c.colors.tabs.selected.odd.fg = xresources['*color15']

c.colors.tabs.pinned.even.bg = xresources['*color2']
c.colors.tabs.pinned.odd.bg = xresources['*color10']
c.colors.tabs.pinned.even.fg = xresources['*fadeColor']
c.colors.tabs.pinned.odd.fg = xresources['*fadeColor']

c.colors.tabs.pinned.selected.even.bg = xresources['*foreground']
c.colors.tabs.pinned.selected.odd.bg = xresources['*foreground']
c.colors.tabs.pinned.selected.even.fg = xresources['*color15']
c.colors.tabs.pinned.selected.odd.fg = xresources['*color15']

c.colors.statusbar.progress.bg = xresources['*cursorColor']
c.colors.statusbar.normal.bg = xresources['*background']
c.colors.statusbar.normal.fg = xresources['*foreground']
c.colors.statusbar.command.bg = xresources['*background']
c.colors.statusbar.command.fg = xresources['*foreground']

c.colors.statusbar.url.fg = xresources['*cursorColor']
c.colors.statusbar.url.success.http.fg = xresources['*cursorColor']
c.colors.statusbar.url.success.https.fg = xresources['*color2']

c.colors.prompts.bg = xresources['*background']
c.colors.prompts.fg = xresources['*foreground']
c.colors.prompts.selected.fg = xresources['*fadeColor']
c.colors.prompts.selected.bg = xresources['*foreground']
c.colors.prompts.border = xresources['*foreground']

c.colors.messages.error.bg = xresources['*color1']
c.colors.messages.error.fg = xresources['*color7']
c.colors.messages.warning.bg = xresources['*color9']
c.colors.messages.warning.fg = xresources['*fadeColor']
c.colors.messages.info.bg = xresources['*fadeColor']
c.colors.messages.info.fg = xresources['*color7']

# c.colors.hints.bg = xresources['*color11']
# c.colors.hints.fg = xresources['*fadeColor']
# c.colors.hints.match.fg = xresources['*color4']

# c.colors.keyhint.bg = xresources['*background']
c.colors.keyhint.fg = xresources['*cursorColor']
c.colors.keyhint.suffix.fg = xresources['*color3']

c.colors.completion.scrollbar.bg = xresources['*background']
c.colors.completion.scrollbar.fg = xresources['*foreground']
c.colors.completion.even.bg = xresources['*background']
c.colors.completion.odd.bg = xresources['*fadeColor']
c.colors.completion.fg = xresources['*cursorColor']
c.colors.completion.category.fg = xresources['*fadeColor']
c.colors.completion.category.bg = xresources['*foreground']
c.colors.completion.category.border.top = xresources['*foreground']
c.colors.completion.category.border.bottom = xresources['*foreground']
c.colors.completion.item.selected.fg = xresources['*fadeColor']
c.colors.completion.item.selected.bg = xresources['*foreground']
c.colors.completion.item.selected.border.top = xresources['*foreground']
c.colors.completion.item.selected.border.bottom = xresources['*foreground']
