# Documentation:
#   qute://help/configuring.html
#   qute://help/settings.html

config.load_autoconfig(False)
config.source('theme.py')
config.source('searchengines.py')
config.source('redirect.py')

c.fonts.default_size = '11pt'
c.colors.webpage.preferred_color_scheme = 'dark'
c.content.blocking.method = 'both'
c.content.default_encoding = 'utf-8'
c.url.open_base_url = True

with config.pattern("*://crt.sh/*") as p:
    c.content.headers.user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.53 Safari/537.36'


with config.pattern("*://temp-mail.org/*") as p:
    c.content.headers.user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.53 Safari/537.36'


c.editor.command = ['termedit', '{file}', '-c', 'normal {line}G{column0}l']

config.bind(',v', 'spawn -d mpvstream {url}')
config.bind(';v', 'hint links spawn -d mpvstream {hint-url}')

config.bind(',a', 'spawn -d mpc-add-web -p {url}')
config.bind(';a', 'hint links spawn -d mpc-add-web -p {hint-url}')

c.aliases = {
    'q': 'close',
    'qa': 'quit',
    'qf': 'history-clear -f ;; spawn qb-clearcookies ;; quit',
    'w': 'session-save',
    'wq': 'quit --save',
    'wqa': 'quit --save',
    'clear': 'history-clear -f ;; spawn qb-clearcookies',
    'cookies-clear': 'spawn qb-clearcookies'
}
