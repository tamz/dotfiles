import qutebrowser.api.interceptor
import sys
import re


# adapted from https://github.com/Xian-beep/qutebrowser-redirect
rewrites = {
        r'(www\.|old\.|new\.|np\.)?reddit\.com': 'teddit.net',
        r'(www\.|mobile\.|m\.)?twitter\.com': 'nitter.net',
        r'(www\.)?instagram\.com': 'bibliogram.art',
        r'en\.m\.wikipedia\.org': 'en.wikipedia.org'
}


def rewrite(request: qutebrowser.api.interceptor.Request):

    for k, v in rewrites.items():
        expr = re.compile(k)
        if expr.fullmatch(request.request_url.host()):
            request.request_url.setHost(v)
            try:
                request.redirect(request.request_url)
            except e:
                print(f'redirect to {v} failed', file=sys.stderr)


qutebrowser.api.interceptor.register(rewrite)
